
CREATE DATABASE [PruebaDaleDb2]
GO
USE [PruebaDaleDb2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[IdClient] [int] IDENTITY(1,1) NOT NULL,
	[Cedula] [varchar](50) NULL,
	[Name] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Phone] [varchar](100) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[IdClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[IdPorduct] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[UnitValue] [decimal](18, 2) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[IdPorduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sale]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sale](
	[IdSale] [int] IDENTITY(1,1) NOT NULL,
	[IdProduct] [int] NOT NULL,
	[IdClient] [int] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[UnitValue] [decimal](18, 2) NULL,
	[TotalValue] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Sell] PRIMARY KEY CLUSTERED 
(
	[IdSale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_Sell_Client] FOREIGN KEY([IdClient])
REFERENCES [dbo].[Client] ([IdClient])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_Sell_Client]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_Sell_Product] FOREIGN KEY([IdProduct])
REFERENCES [dbo].[Product] ([IdPorduct])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_Sell_Product]
GO
/****** Object:  StoredProcedure [dbo].[SP_CreateUpdateClient]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_CreateUpdateClient] 
 @IdClient		INT = 0 ,
 @Cedula	 varchar(100),
 @Name	 varchar(100),
 @LastName	 varchar(100),
 @Phone	 varchar(100),
 @Active BIT = 1

AS
BEGIN

IF @IdClient = 0
	BEGIN 
		INSERT [dbo].Client(Cedula, Name, LastName, Phone, Active) values(@Cedula,@Name,@LastName,@Phone, @Active)
	END
ELSE
	BEGIN
		UPDATE  [dbo].Client SET 
			Cedula = @Cedula ,
			Name = @Name ,
			LastName = @LastName ,
			Phone = @Phone,
			Active = @Active
		WHERE IdClient = @IdClient
	END


select SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CreateUpdateProduct]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_CreateUpdateProduct] 
 @IdPorduct		INT = 0 ,
 @Name	 varchar(100),
 @UnitValue		 Decimal(18,2),
  @Active BIT = 1

AS
BEGIN

IF @IdPorduct = 0
	BEGIN 
		INSERT [dbo].Product(Name, UnitValue,Active) values(@Name,@UnitValue,@Active)
	END
ELSE
	BEGIN
		UPDATE  [dbo].Product SET Name = @Name , UnitValue= @UnitValue, Active = @Active
		WHERE IdPorduct = @IdPorduct
	END


select SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[SP_CreateUpdatesale]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_CreateUpdatesale] 
 @IdSale		INT = 0 ,
  @IdProduct		INT ,
   @IdClient		INT  ,
 @Amount	 decimal(18,2),
 @UnitValue	 decimal(18,2),
 @TotalValue	decimal(18,2)

AS
BEGIN

IF @IdSale = 0
	BEGIN 
		INSERT [dbo].Sale(IdProduct, IdClient, Amount, UnitValue, TotalValue) 
		values(@IdProduct,@IdClient,@Amount,@UnitValue, @TotalValue)
	END
ELSE
	BEGIN
		UPDATE  [dbo].Sale SET 
			IdProduct = @IdProduct ,
			IdClient = @IdClient ,
			Amount = @Amount ,
			UnitValue = @UnitValue ,
			TotalValue = @TotalValue 
		WHERE IdSale = @IdSale
	END


select SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteClient]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_DeleteClient] 
 @IdClient		INT 

AS
BEGIN
			DELETE  [dbo].Client 
			WHERE IdClient = @IdClient
			select SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteProduct]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_DeleteProduct] 
 @IdPorduct		INT 

AS
BEGIN
			DELETE  [dbo].Product 
			WHERE IdPorduct = @IdPorduct
			select SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetClient]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetClient] 
 @IdClient int

AS
BEGIN
			SELECT  
				IdClient,
				Cedula, 
				Name, 
				LastName,
				Phone,
				Active
			FROM [dbo].Client
			WHERE IdClient = @IdClient
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetProduct]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetProduct] 
 @IdPorduct int

AS
BEGIN
			SELECT  
				IdPorduct,
				Name,
				UnitValue,
				Active
			FROM [dbo].Product
			WHERE IdPorduct = @IdPorduct
END
GO
/****** Object:  StoredProcedure [dbo].[SP_ListClients]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ListClients] 
 

AS
BEGIN
			SELECT  
				IdClient,
				Cedula, 
				Name, 
				LastName,
				Phone,
				Active
			FROM [dbo].Client
END
GO
/****** Object:  StoredProcedure [dbo].[SP_ListProducts]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ListProducts] 
 

AS
BEGIN
			SELECT  
				IdPorduct,
				Name,
				UnitValue,
				Active
				FROM [dbo].Product
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Listsales]    Script Date: 7/2/2021 12:00:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Listsales] 
 

AS
BEGIN
			SELECT  
				IdSale,
				C.IdClient,
				C.Name + ' '  + C.LastName  NameClient,
				IdProduct,
				P.Name NameProduct,
				Amount,
				S.UnitValue,
				TotalValue
				FROM [dbo].Sale S with (nolock)
				JOIN [dbo].Client C with (nolock) ON C.IdClient = S.IdClient
				JOIN [dbo].Product P with (nolock) ON P.IdPorduct = S.IdProduct
END

